import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { AuthFormsModule } from '../../../../../shared/src/lib';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';



@NgModule({
  declarations: [
    LoginPageComponent,
    RegisterPageComponent
  ],
    imports: [
        CommonModule,
        AuthFormsModule,
        RouterModule,
        MatProgressSpinnerModule
    ],
  exports: [
    LoginPageComponent,
    RegisterPageComponent
  ]
})
export class AuthPagesModule { }
