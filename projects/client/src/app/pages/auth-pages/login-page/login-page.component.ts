import { Component } from '@angular/core';
import { LoginFormI } from '@shared/lib';
import { AuthService } from '@client/app/services/auth.service';
import { BehaviorSubject, finalize } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'cl-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent {
  isLoading$ = new BehaviorSubject<boolean>(false);

  constructor(
    private readonly authSrv: AuthService,
    private readonly router: Router
  ) {
  }

  login(data: LoginFormI): void {
    this.authSrv.signIn(data.email, data.password)
      .pipe(
        finalize(() => this.isLoading$.next(false))
      )
      .subscribe(() => {
        this.router.navigateByUrl('/')
      })
  }
}
