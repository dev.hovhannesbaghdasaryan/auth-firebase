import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RegisterFormI } from '@shared/lib';
import { AuthService } from '@client/app/services/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, finalize, switchMap } from 'rxjs';

@Component({
  selector: 'cl-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterPageComponent {
  isLoading$ = new BehaviorSubject<boolean>(false);
  constructor(
    private readonly authSrv: AuthService,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar
  ) {
  }

  register(data: RegisterFormI): void {
    this.isLoading$.next(true);
    this.authSrv.signUp(data.email, data.password, 'client', data.name)
      .pipe(
        switchMap(() => this.authSrv.sendVerificationMail()),
        finalize(() => this.isLoading$.next(false))
      )
      .subscribe({
        next: () => this.router.navigateByUrl('/'),
        error: (e) => {
          console.log(e);
          this.snackBar.open('Something went wrong')
        }
      })
  }
}
