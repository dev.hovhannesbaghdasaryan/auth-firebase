import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthPagesModule } from '../../pages/auth-pages/auth-pages.module';
import { LoginPageComponent } from '../../pages/auth-pages/login-page/login-page.component';
import { RegisterPageComponent } from '../../pages/auth-pages/register-page/register-page.component';
import { AuthComponent } from '@client/app/layouts/auth/auth.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AuthPagesModule,
    RouterModule.forChild([
      {
        path: '',
        component: AuthComponent,
        children: [
          {
            path: 'login',
            component: LoginPageComponent
          },
          {
            path: 'register',
            component: RegisterPageComponent
          }
        ]
      }
    ])
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
