import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthPagesModule } from '../../pages/auth-pages/auth-pages.module';
import { LoginPageComponent } from '../../pages/auth-pages/login-page/login-page.component';
import { AuthComponent } from '@admin/app/layouts/auth/auth.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AuthPagesModule,
    RouterModule.forChild([
      {
        path: '',
        component: AuthComponent,
        children: [
          {
            path: 'login',
            component: LoginPageComponent
          }
        ]
      }
    ])
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
