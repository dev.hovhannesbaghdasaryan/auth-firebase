import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthFormsModule } from '@shared/lib';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';



@NgModule({
  declarations: [
    LoginPageComponent,
  ],
    imports: [
        CommonModule,
        AuthFormsModule,
        RouterModule,
        MatProgressSpinnerModule
    ],
  exports: [
    LoginPageComponent,
  ]
})
export class AuthPagesModule { }
