import { Injectable } from '@angular/core';
import { UserI, UserType } from '@shared/lib/interfaces/user.interface';
import { AUTH_DATA_KEY, AUTH_USER_KEY } from '@shared/lib';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { from, map, Observable, of, switchMap, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthBaseService {
  constructor(
    protected readonly afs: AngularFirestore, // Inject Firestore service
    protected readonly afAuth: AngularFireAuth, // Inject Firebase auth service
  ) {
    /* Saving user data in localstorage when
    logged in and setting up null when logged out */
    this.afAuth.authState
      .pipe(
        switchMap(authData => {
          if (!authData) {
            return of(null);
          }
          const userRef: AngularFirestoreDocument<any> = this.afs.doc(
            `users/${authData.uid}`
          );
          return userRef.get().pipe(
            map(doc => {
              if (doc.data()) {
                this.user = doc.data();
              }
              return authData;
            })
          )
        }),
      )
      .subscribe((authData) => {
        if (authData) {
          this.authData = authData;
        } else {
          localStorage.removeItem(AUTH_USER_KEY);
        }
    });
  }

  private _authData: any = localStorage.getItem(AUTH_DATA_KEY);
  get authData() {
    return this._authData;
  }
  set authData(vale: any) {
    this._authData = vale;
    localStorage.setItem(AUTH_DATA_KEY, JSON.stringify(vale));
  }

  private _user!: UserI;
  get user(): UserI {
    return this._user;
  }
  set user(value: UserI) {
    this._user = value;
  }

  // Sign in with email/password
  signIn(email: string, password: string): Observable<UserI> {
    return from(this.afAuth
      .signInWithEmailAndPassword(email, password))
      .pipe(
        // @ts-ignore
        tap(result => this.user = result.user as UserI),
        map(() => this.user)
      )
  }
  // Sign up with email/password
  signUp(email: string, password: string, userType: UserType, name: string): Observable<UserI> {
    return from(this.afAuth.createUserWithEmailAndPassword(email, password))
      .pipe(
        switchMap(res => this.setUserData(res.user, userType, name))
      );
  }

  // Send email verification when new user sign up
  sendVerificationMail(): Observable<UserI> {
    return from(this.afAuth.currentUser).pipe(
      switchMap(u => u
        ? from(u.sendEmailVerification()).pipe(
          map(() => this.user)
        )
        : of(this.user)
      )
    );
  }

  // Reset Forgot password
  forgotPassword(passwordResetEmail: string) {
    return from(this.afAuth.sendPasswordResetEmail(passwordResetEmail));
  }

  /* Setting up user data when sign in with username/password,
  sign up with username/password and sign in with social auth
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  setUserData(user: any, userType: UserType, name: string): Observable<UserI> {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${user.uid}`
    );
    const userData: UserI = {
      uid: user.uid,
      email: user.email,
      displayName: name,
      type: userType,
      emailVerified: user.emailVerified,
    };
    return from(userRef.set(userData, {
      merge: true,
    })).pipe(
      map(() => userData)
    );
  }

  // Sign out
  signOut(): Observable<void> {
    return from(this.afAuth.signOut()).pipe(
      tap(() => localStorage.removeItem(AUTH_USER_KEY))
    )
  }

  isLoggedIn(): Observable<boolean> {
    return this.afAuth.user.pipe(
      map(u => !!u),
      tap(u => console.log(u))
    )
  }
}
