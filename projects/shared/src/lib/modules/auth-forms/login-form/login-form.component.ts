import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginFormI } from '@shared/lib';

@Component({
  selector: 'lib-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginFormComponent {
  @Output() submitted = new EventEmitter<LoginFormI>();

  readonly form: FormGroup;
  showPassword: boolean = false;

  constructor(
    private readonly fb: FormBuilder
  ) {
    this.form = this.fb.group({
      'email': new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$'),
        Validators.minLength(4),
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32),
      ])
    })
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;
  }
}
