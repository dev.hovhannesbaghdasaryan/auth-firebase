import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { passwordMatchingValidator } from '@shared/lib';
import { RegisterFormI } from '@shared/lib';

@Component({
  selector: 'lib-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterFormComponent {
  @Output() submitted = new EventEmitter<RegisterFormI>();

  readonly form: FormGroup;
  showPassword: boolean = false;

  constructor(
    private readonly fb: FormBuilder
  ) {
    this.form = this.fb.group({
      'name': new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(64),
      ]),
      'email': new FormControl('', [
        Validators.required,
        Validators.pattern('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$'),
        Validators.minLength(4),
        Validators.maxLength(32),
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32),
      ]),
      'confirmPassword': new FormControl('', [Validators.required])
    }, {
      validator: passwordMatchingValidator('password', 'confirmPassword')
    })
  }
  togglePassword(): void {
    this.showPassword = !this.showPassword;
  }
}
