export interface LoginFormI {
  email: string;
  password: string;
}

export interface RegisterFormI extends LoginFormI {
  name: string;
}
