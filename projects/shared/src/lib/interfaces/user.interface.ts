export type UserType = 'client' | 'customer' | 'admin';

export interface UserI {
  uid: string;
  email: string;
  displayName: string;
  type: UserType;
  emailVerified: boolean;
}
