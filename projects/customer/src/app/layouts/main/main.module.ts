import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';



@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule
  ]
})
export class MainModule { }
