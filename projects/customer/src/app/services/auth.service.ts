import { Injectable } from '@angular/core';
import { AuthBaseService } from '@shared/lib';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends AuthBaseService {

}
